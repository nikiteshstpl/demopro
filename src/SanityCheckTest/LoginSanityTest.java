package SanityCheckTest;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.io.IOException;




import Pages.BasePage;
import Pages.LoginPage;
import Pages.HomePage;


public class LoginSanityTest extends BasePage{
 
	
	@BeforeClass
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	
	@Test
	public void verifyValidLoginTest() throws InterruptedException, IOException{
		        System.out.println("Inside Login Test : Trying to Login");
				LoginPage.login("CalicomTestUser@yopmail.com", "123456@123");
				waitForPageLoad(10);
				assertTrue(HomePage.isHomePageLoaded());
				captureScreenshot("LoginSuccessful");	
				System.out.println("Login Credentials entered successfully");		
		
	}
		
	
	@AfterClass
	public void CloseSetUp(){
		System.out.println("Inside CloseSetup");
		closeSession();
		System.out.println("CloseSetup Done");
		
	}
	
}
