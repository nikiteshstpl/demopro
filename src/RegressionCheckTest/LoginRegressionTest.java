package RegressionCheckTest;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Pages.BasePage;
import Pages.HomePage;
import Pages.LoginPage;

public class LoginRegressionTest extends BasePage {
 

	@BeforeClass
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	@Test
	public void verifyInvalidLoginCredentialTest() throws InterruptedException, IOException{
		        System.out.println("Inside verify Invalid Login Credential Test : Trying to Login with invalid login credentials");
				LoginPage.login("CalicomTest@yopmail.com","123456@12");
				assertTrue(LoginPage.isInvalidEmailPasswordValidationPresent());
				captureScreenshot("InvalidEmailPasswordValidation");	
				System.out.println("Login Credentials are invalid");		
		
	}
	
	@AfterClass
	public void CloseSetUp(){
		System.out.println("Inside CloseSetup");
		closeSession();
		System.out.println("CloseSetup Done");
		
	}
}
