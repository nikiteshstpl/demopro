package Pages;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {
	
	public final static By byUserNameFeild = findBy("//input[@placeholder='Username or Email']"); 
	public final static By byPasswordFeild = findBy("//input[@placeholder='Password']"); 
	public final static By bySignInButton = findBy("//button[contains(text(),'SIGN IN')]"); 
	public final static By bySignInWithGoogleButton = findBy("//button[contains(text(),'Sign In with Google')]"); 
	public final static By byCreateAccountLink = findBy("//a[contains(text(),'Create an Account ?')]"); 
	public final static By byForgotPasswordLink= findBy("//a[contains(text(),'Forgot Password ?')]"); 
	public final static By byEmailPasswordValidation = findBy("//div[contains(text(),'Please enter user name / email and password')]"); 
	public final static By byInvalidEmailPasswordValidation = findBy("//div[contains(text(),'Invalid username/email or password')]");
	public final static By byEnterPasswordValidation = findBy("//div[contains(text(),'Please enter password')]");
	
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////
	
	
	public static boolean isLoginPageLoaded(){
		return isElementPresent(byCreateAccountLink, "Create Account link");	
	}
	
	public static boolean isUserNamePresent(){
	return isElementPresent(byUserNameFeild, "Username Feild");
	
	}
	
	public static boolean isPasswordFeildPresent(){
		return isElementPresent(byPasswordFeild, "Password Feild");
		
	}
	
	public static boolean isSignInButtonPresent(){
		return isElementPresent(bySignInButton, "SignIn Button");
		
	}
	
	public static boolean isSignInWithGoogleButtonPresent(){
		return isElementPresent(bySignInWithGoogleButton, "SignIn With Google Button");
		
	}
	
	
	public static boolean isCreateAccountLinkPresent(){
		return isElementPresent(byCreateAccountLink, "Create Account link");
		
	}
	
	public static boolean isForgotPasswordLinkPresent(){
		return isElementPresent(byForgotPasswordLink, "Forgot password link");
		
	}
	
	public static boolean isEmailPasswordValidationPresent(){
		return isElementPresent(byEmailPasswordValidation, "Blank Email Password Validation");
		
	}
	
	public static boolean isInvalidEmailPasswordValidationPresent(){
		return isElementPresent(byInvalidEmailPasswordValidation, "Invalid Email Password Validation");
		
	}
	
	public static boolean isEnterPasswordValidationPresent(){
		return isElementPresent(byEnterPasswordValidation, "Blank Password Validation");
		
	}
	
	
	
         ////////////////////////////////////////////////////////////
         //////////////////////  Setters ///////////////////////////
         ///////////////////////////////////////////////////////////
	
	
	public static void setUserNameValue(String sText){
		setText(byUserNameFeild,"Username Feild",sText);
		
	}
	
	public static void setPasswordValue(String sText){
		setText(byPasswordFeild, "Pssword Feild",sText);
	}	
	
	

	 ////////////////////////////////////////////////////////////
    //////////////////////  Clickers ///////////////////////////
    ///////////////////////////////////////////////////////////
	
	
	public static void clickSignIn(){
		
		clickElement(bySignInButton, "Sign In Button");

	}
	
	public static void clickCreateAccount(){
		
		clickElement(byCreateAccountLink, "Create Account link");

	}
	
	public static void clickForgotPassword(){
		
		clickElement(byForgotPasswordLink, "ForgotPasswrod  link");

	}
	
	
	public static void clickSignInWithGoogleButton(){
		
		clickElement(bySignInWithGoogleButton, "SignIn With Google Button");

	}
	
	 ////////////////////////////////////////////////////////////
    /////////////////////  Helper Methods /////////////////////
    ///////////////////////////////////////////////////////////
	
	public static void login(String sUserName, String sPassword){
		
		setUserNameValue(sUserName);
		setPasswordValue(sPassword);
		clickSignIn();
	}
	
	
	
	
	
}
