package Pages;

import org.openqa.selenium.By;

public class CreateAccountPage extends BasePage {

	public final static By byUserNameFeild = findBy("//input[@placeholder='Username']"); 
	public final static By byEmailFeild = findBy("//input[@placeholder='Email']"); 
	public final static By byPasswordFeild = findBy("//input[@placeholder='Password']"); 
	public final static By bySignInButton = findBy("//button[contains(text(),'SIGN IN')]"); 
	public final static By bySignInWithGoogleButton = findBy("//button[contains(text(),'Sign In with Google')]"); 
	
	
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////
	
	
	public static boolean isCreateAccountPageLoaded(){
		return isElementPresent(byEmailFeild, "Email ID Feild");	
	}
	
	public static boolean isUserNamePresent(){
	return isElementPresent(byUserNameFeild, "Username Feild");
	
	}
	
	public static boolean isPasswordFeildPresent(){
		return isElementPresent(byPasswordFeild, "Password Feild");
		
	}
	
	public static boolean isSignInButtonPresent(){
		return isElementPresent(bySignInButton, "SignIn Button");
		
	}
	
	public static boolean isSignInWithGoogleButtonPresent(){
		return isElementPresent(bySignInWithGoogleButton, "SignIn With Google Button");
		
	}
	
	
	
	
	
}
