package Pages;

import java.io.File;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
	
	 public static WebDriverWait wait;
	 public static WebDriver driver;

	 public void InitialConfig() {
		 
	        driver = new FirefoxDriver();
	        driver.get("https://lom-test-web.azurewebsites.net/account/login?r=/");
	        driver.manage().window().maximize();
	        waitForPageLoad(5);
	    }	    

     public static By findBy(String sLocator)
	    {
	        By byElement = null;
	        if (sLocator.contains("/"))
	        {
	            byElement = By.xpath(sLocator);
	        }
	        else{
	        	   byElement= By.id(sLocator);
	       
	        }
	        return byElement;    
	 }	          
 
	
     public static WebElement findObject(By ele, String selectorName )
     {      WebElement	rClientElement = null;  
     try{
    	 	rClientElement = driver.findElement(ele);
    	 	
         } 
         catch(Exception e){
        	 System.out.println ("ERROR: Element "+selectorName+"  not found");
         }
        
     return rClientElement;
    }
     
     
  
     public static List<WebElement> findElements(By ele, String selector)
     {

         List<WebElement> lsWebElement;
         lsWebElement = driver.findElements(ele);
         if (lsWebElement.size()==0)
             System.out.println("ERROR: Expected "+selector+" found No element ");
        return lsWebElement;

     }

     public static void navigateBack()
     {
         driver.navigate().back();
     }
     

     public static void setText(By ele, String selector ,String sText)
     {	
         WebElement wEle = findObject(ele, selector);
         wEle.sendKeys(sText);
     }
     
     
     public static boolean isElementPresent(By ele, String selector)
     {	
    	 boolean status= false;
         WebElement wEle = findObject(ele, selector);
         
        try{
        status=wEle.isDisplayed();
        return status;
        }catch(Exception e){
        	 System.out.println("ERROR: Element "+selector+" is not present on screen ");
        }
        return status;
     }

     public static boolean isElementEnabled(By ele, String selector)
     {	
    	 boolean status= false;
         WebElement wEle = findObject(ele, selector);
         
        try{
        status=wEle.isEnabled();
        return status;
        }catch(Exception e){
        	 System.out.println("ERROR: Element "+selector+" is not Enabled on screen ");
        }
        return status;
     }
     
     
     public static void scrollPage(By ele, String selector){
    	 WebElement wEle = findObject(ele, selector);
    	 
    	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wEle);
     }
     
     
     public static void clickElement(By ele, String selector)
     {
         WebElement wEle = findObject(ele, selector);
        try {
            wEle.click();
        }catch (Exception e)
        {
        	 System.out.println("ERROR: Element "+selector+" is unclickable ");
        }

     }
     
     public void closeSession() {
         driver.quit();
     }
     
     
     public static void waitForPageLoad(int iTimeUnit){
    	 driver.manage().timeouts().implicitlyWait(iTimeUnit, TimeUnit.SECONDS);
    	 
     }
     
     public static void sleepApplication(int iTimeUnit){
    	 
    	 try {
			Thread.sleep(iTimeUnit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
     
	    
     public static void captureScreenshot(String fileName) throws IOException {
         TakesScreenshot ts= (TakesScreenshot)driver;
         File src=ts.getScreenshotAs(OutputType.FILE); 
         FileUtils.copyFile(src, new File("./ScreenCapture/"+fileName+".png"));
     }
     

     
     
  }
	 


