package Pages;

import org.openqa.selenium.By;

public class HomePage extends BasePage{
	public final static By bySearchBox = findBy("//input[@placeholder='Search....']"); 
	public final static By bySearchButton = findBy("//button[@ng-click='vm.gotoSearch()']"); 
	public final static By byUploadLink = findBy("//a[text()='+ Upload']"); 
	public final static By byCalicomLogo = findBy("//img[@title='Calicom - Video Library Logo']"); 
	public final static By byUserProfileSettingButton = findBy("//a[@class='profile-picture ng-binding']");
	public final static By byPublicVideosLink = findBy("//h3[contains(text(),'Public Videos')]");
	public final static By byVideoManagerLink = findBy("//h3[contains(text(),'Video Manager')]");
	
	
////////////////////////////////////////////////////////////
//////////////////////  Validators  ////////////////////////
///////////////////////////////////////////////////////////


public static boolean isHomePageLoaded(){
return isElementPresent(bySearchBox, "Search Box Feild");	
}

public static boolean isSearchBoxPresent(){
return isElementPresent(bySearchBox, "Search Box Feild");

}

public static boolean isSearchButtonPresent(){
return isElementPresent(bySearchButton, "Search button Feild");

}

public static boolean isUploadLinkPresent(){
return isElementPresent(byUploadLink, "Upload Link");

}

public static boolean isCalicomLogoPresent(){
return isElementPresent(byCalicomLogo, "Calicom Logo");

}

public static boolean isUserProfileSettingButtonPresent(){
return isElementPresent(byUserProfileSettingButton, "User Profile Setting Button");

}

public static boolean isPublicVideosLinkPresent(){
return isElementPresent(byPublicVideosLink, "Public Videos Link");

}

public static boolean isVideoManagerLinkPresent(){
return isElementPresent(byVideoManagerLink, "Video Manager Link");

}

	
	
}
